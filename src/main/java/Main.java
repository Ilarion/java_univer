import com.google.gson.*;
import filters.Count;
import helpers.Deserialization;
import helpers.JsonReader;
import helpers.JsonWriter;

public class Main {

    public static void main(String[] args) throws Exception {
        String path1 = "resources/document.txt";
        String path2 = "resources/document2.txt";

        JsonObject jsonObject = JsonReader.getJsonObject(path1);
        System.out.println(jsonObject);
        JsonObject country = Deserialization.getCountry(jsonObject);
        System.out.println(country);
        JsonArray regions = Deserialization.getRegions(country);
        System.out.println(regions);
        JsonArray citiesArray = Deserialization.getCities(regions);
        System.out.println(citiesArray);
        JsonArray personsArray = Deserialization.getPersons(citiesArray);
        System.out.println(personsArray);


        // get amount of persons, cities, regions
        long population = Count.getPopulation(personsArray);
        long citiesCount = Count.getCitiesCount(citiesArray);
        long regionsCount = Count.getRegionsCount(regions);

        System.out.println(population);
        System.out.println(citiesCount);
        System.out.println(regionsCount);


        // check that JSON that we read from path1 equals to JSON from path2
        // that we have written using JsonWriter.writeJsonObject()
        JsonWriter.writeJsonObject(jsonObject, path2);
        if (JsonReader.getJsonObject(path1).equals(JsonReader.getJsonObject(path2)))
        System.out.println("json from path1 equals to json from path2");



        // to get a name of a person
       // System.out.println("******" + personsArray.get(0).getAsJsonObject().getAsJsonObject("person1").getAsJsonPrimitive("name").getAsString());
    }
}
