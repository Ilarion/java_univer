package filters;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

/**
 * Created by Admin on 11/11/2017.
 */
public class Count {
    public static long getPopulation(JsonArray persons) {
        Gson googleJson = new Gson();
        ArrayList personsList = googleJson.fromJson(persons, ArrayList.class);
        return personsList.stream().count();
    }

    public static long getRegionsCount(JsonArray regions) {
        Gson googleJson = new Gson();
        ArrayList regionsList = googleJson.fromJson(regions, ArrayList.class);
        return regionsList != null ? regionsList.stream().count() : 0;
    }

    public static long getCitiesCount(JsonArray cities) {
        Gson googleJson = new Gson();
        ArrayList citiesList = googleJson.fromJson(cities, ArrayList.class);
        return citiesList.stream().count();
    }

}
