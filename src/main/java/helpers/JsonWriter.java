package helpers;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * Created by Admin on 11/11/2017.
 */
public class JsonWriter {
    public static void writeJsonObject(JsonObject object, String path) throws Exception {
        PrintWriter writer = new PrintWriter(path, "UTF-8");
        writer.println(object);
        writer.close();
    }
}
