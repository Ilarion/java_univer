package helpers;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.ArrayList;

/**
 * Created by Admin on 11/11/2017.
 */
public class Deserialization {
    public static JsonObject getCountry(JsonObject jsonObject) {
        JsonObject country = new JsonObject();
        try {
            return jsonObject.getAsJsonObject("Ukraine");
        }
        catch (Exception e) {
            System.out.println("Ukraine couldn't be found");
        }
        return country;
    }

    public static JsonArray getRegions(JsonObject country) {
        JsonArray regions = new JsonArray();
        try {
            regions = country.getAsJsonArray("regions");
        }
        catch (Exception e) {
            System.out.println("regions couldn't be found");
        }
        return regions;
    }

    public static JsonArray getCities(JsonArray regions) {
        JsonArray citiesArray = new JsonArray();
        try {
            citiesArray.add(regions.get(0).getAsJsonObject().getAsJsonArray("Dnipro").get(0));
        }
        catch (Exception e) {
            System.out.println("first city in Dnipro couldn't be found");
        }
        try {
            citiesArray.add(regions.get(0).getAsJsonObject().getAsJsonArray("Dnipro").get(1));
        }
        catch (Exception e) {
            System.out.println("second city in Dnipro couldn't be found");
        }
        try {
            citiesArray.add(regions.get(1).getAsJsonObject().getAsJsonArray("Odessa").get(0));
        }
        catch (Exception e) {
            System.out.println("first city in Odessa couldn't be found");
        }
        try {
            citiesArray.add(regions.get(1).getAsJsonObject().getAsJsonArray("Odessa").get(1));
        }
        catch (Exception e) {
            System.out.println("second city in Odessa couldn't be found");
        }
        return citiesArray;
    }

    public static JsonArray getPersons(JsonArray citiesArray) {
        JsonArray personsArray = new JsonArray();
        try {
            personsArray.add(citiesArray.get(0).getAsJsonObject().getAsJsonArray("Novomoskovsk").get(0));
        }
        catch (Exception e) {
            System.out.println("person is missing");
        }
        try {
            personsArray.add(citiesArray.get(0).getAsJsonObject().getAsJsonArray("Novomoskovsk").get(1));
        }
        catch (Exception e) {
            System.out.println("person is missing");
        }
        try {
            personsArray.add(citiesArray.get(1).getAsJsonObject().getAsJsonArray("Nikopol").get(0));
        }
        catch (Exception e) {
            System.out.println("person is missing");
        }
        try {
            personsArray.add(citiesArray.get(1).getAsJsonObject().getAsJsonArray("Nikopol").get(1));
        }
        catch (Exception e) {
            System.out.println("person is missing");
        }
        try {
            personsArray.add(citiesArray.get(2).getAsJsonObject().getAsJsonArray("Chornomorsk").get(0));
        }
        catch (Exception e) {
            System.out.println("person is missing");
        }
        try {
            personsArray.add(citiesArray.get(2).getAsJsonObject().getAsJsonArray("Chornomorsk").get(1));
        }
        catch (Exception e) {
            System.out.println("person is missing");
        }
        try {
            personsArray.add(citiesArray.get(3).getAsJsonObject().getAsJsonArray("Ismail").get(0));
        }
        catch (Exception e) {
            System.out.println("person is missing");
        }
        try {
            personsArray.add(citiesArray.get(3).getAsJsonObject().getAsJsonArray("Ismail").get(1));
        }
        catch (Exception e) {
            System.out.println("person is missing");
        }
        return personsArray;
    }
}
