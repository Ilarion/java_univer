package helpers;

import com.google.gson.*;

import java.io.FileReader;
import java.util.ArrayList;

/**
 * Created by Admin on 11/11/2017.
 */
public class JsonReader {
    public static JsonObject getJsonObject(String path) {
        JsonParser parser = new JsonParser();
        JsonObject jsonObject = new JsonObject();
        try {
            Object obj = parser.parse(new FileReader(path));
            jsonObject = (JsonObject) obj;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
