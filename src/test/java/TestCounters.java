import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import data.Data;
import filters.Count;
import helpers.Deserialization;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 * Created by Admin on 11/11/2017.
 */
public class TestCounters {

    @Test(dataProvider = "amountOfCities", dataProviderClass = Data.class)
    public void amountOfCities (int actualAmountOfCities, JsonObject jsonObject) {
        JsonObject country = Deserialization.getCountry(jsonObject);
        JsonArray regions = Deserialization.getRegions(country);
        JsonArray cities = Deserialization.getCities(regions);

        long amountOfCities = Count.getCitiesCount(cities);
        assertEquals(amountOfCities, actualAmountOfCities,
                "Amount of cities should equal " + actualAmountOfCities + ", but it is " + amountOfCities);
    }

    @Test(dataProvider = "amountOfRegions", dataProviderClass = Data.class)
    public void amountOfRegions (int actualAmountOfRegions, JsonObject jsonObject) {
        JsonObject country = Deserialization.getCountry(jsonObject);
        JsonArray regions = Deserialization.getRegions(country);

        long amountOfRegions = Count.getRegionsCount(regions);
        assertEquals(amountOfRegions, actualAmountOfRegions,
                "Amount of regions should equal " + actualAmountOfRegions + ", but it is " + amountOfRegions);
    }

    @Test(dataProvider = "amountOfPersons", dataProviderClass = Data.class)
    public void amountOfPersons (int actualAmountOfPersons, JsonObject jsonObject) {
        JsonObject country = Deserialization.getCountry(jsonObject);
        JsonArray regions = Deserialization.getRegions(country);
        JsonArray cities = Deserialization.getCities(regions);
        JsonArray persons = Deserialization.getPersons(cities);

        long amountOfPersons = Count.getPopulation(persons);
        assertEquals(amountOfPersons, actualAmountOfPersons,
                "Amount of regions should equal " + actualAmountOfPersons + ", but it is " + amountOfPersons);
    }
}
