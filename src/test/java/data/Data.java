package data;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.testng.annotations.DataProvider;

/**
 * Created by Admin on 11/11/2017.
 */
public class Data {
    @org.testng.annotations.DataProvider(name = "amountOfCities")
    public static Object[][] createDataCities() {
        return new Object[][] {
                {4, (JsonObject) new JsonParser().parse("{\"Ukraine\":{\"capital\":\"Kiev\",\"mayor\":\"Petro\",\"regions\":[{\"Dnipro\":[{\"Novomoskovsk\":[{\"person1\":{\"name\":\"Nadya\",\"age\":\"25\",\"sex\":\"F\"}},{\"person2\":{\"name\":\"Vasya\",\"age\":\"25\",\"sex\":\"M\"}}]},{\"Nikopol\":[{\"person3\":{\"name\":\"Valya\",\"age\":\"25\",\"sex\":\"F\"}},{\"person4\":{\"name\":\"Kolya\",\"age\":\"25\",\"sex\":\"M\"}}]}]},{\"Odessa\":[{\"Chornomorsk\":[{\"person5\":{\"name\":\"Dina\",\"age\":\"25\",\"sex\":\"F\"}},{\"person6\":{\"name\":\"David\",\"age\":\"25\",\"sex\":\"M\"}}]},{\"Ismail\":[{\"person7\":{\"name\":\"Edna\",\"age\":\"25\",\"sex\":\"F\"}},{\"person8\":{\"name\":\"Abram\",\"age\":\"25\",\"sex\":\"M\"}}]}]}]}}")},
                {2, (JsonObject) new JsonParser().parse("{ \"Ukraine\": { \"capital\": \"Kiev\", \"mayor\": \"Petro\", \"regions\": [{ \"Dnipro\": [{ \"Novomoskovsk\": [{ \"person1\": { \"name\": \"Nadya\", \"age\": \"25\", \"sex\": \"F\" } }, { \"person2\": { \"name\": \"Vasya\", \"age\": \"25\", \"sex\": \"M\" } }] }, { \"Nikopol\": [{ \"person3\": { \"name\": \"Valya\", \"age\": \"25\", \"sex\": \"F\" } }, { \"person4\": { \"name\": \"Kolya\", \"age\": \"25\", \"sex\": \"M\" } }] }] }] } }")},
                {0, (JsonObject) new JsonParser().parse("{ \"Ukraine\": { \"capital\": \"Kiev\"}}")},
        };
    }

    @org.testng.annotations.DataProvider(name = "amountOfRegions")
    public static Object[][] createDataRegions() {
        return new Object[][] {
                {2, (JsonObject) new JsonParser().parse("{\"Ukraine\":{\"capital\":\"Kiev\",\"mayor\":\"Petro\",\"regions\":[{\"Dnipro\":[{\"Novomoskovsk\":[{\"person1\":{\"name\":\"Nadya\",\"age\":\"25\",\"sex\":\"F\"}},{\"person2\":{\"name\":\"Vasya\",\"age\":\"25\",\"sex\":\"M\"}}]},{\"Nikopol\":[{\"person3\":{\"name\":\"Valya\",\"age\":\"25\",\"sex\":\"F\"}},{\"person4\":{\"name\":\"Kolya\",\"age\":\"25\",\"sex\":\"M\"}}]}]},{\"Odessa\":[{\"Chornomorsk\":[{\"person5\":{\"name\":\"Dina\",\"age\":\"25\",\"sex\":\"F\"}},{\"person6\":{\"name\":\"David\",\"age\":\"25\",\"sex\":\"M\"}}]},{\"Ismail\":[{\"person7\":{\"name\":\"Edna\",\"age\":\"25\",\"sex\":\"F\"}},{\"person8\":{\"name\":\"Abram\",\"age\":\"25\",\"sex\":\"M\"}}]}]}]}}")},
                {1, (JsonObject) new JsonParser().parse("{ \"Ukraine\": { \"capital\": \"Kiev\", \"mayor\": \"Petro\", \"regions\": [{ \"Dnipro\": [{ \"Novomoskovsk\": [{ \"person1\": { \"name\": \"Nadya\", \"age\": \"25\", \"sex\": \"F\" } }, { \"person2\": { \"name\": \"Vasya\", \"age\": \"25\", \"sex\": \"M\" } }] }, { \"Nikopol\": [{ \"person3\": { \"name\": \"Valya\", \"age\": \"25\", \"sex\": \"F\" } }, { \"person4\": { \"name\": \"Kolya\", \"age\": \"25\", \"sex\": \"M\" } }] }] }] } }")},
                {0, (JsonObject) new JsonParser().parse("{ \"Ukraine\": { \"capital\": \"Kiev\"}}")},
        };
    }

    @org.testng.annotations.DataProvider(name = "amountOfPersons")
    public static Object[][] createDataPersons() {
        return new Object[][] {
                {8, (JsonObject) new JsonParser().parse("{\"Ukraine\":{\"capital\":\"Kiev\",\"mayor\":\"Petro\",\"regions\":[{\"Dnipro\":[{\"Novomoskovsk\":[{\"person1\":{\"name\":\"Nadya\",\"age\":\"25\",\"sex\":\"F\"}},{\"person2\":{\"name\":\"Vasya\",\"age\":\"25\",\"sex\":\"M\"}}]},{\"Nikopol\":[{\"person3\":{\"name\":\"Valya\",\"age\":\"25\",\"sex\":\"F\"}},{\"person4\":{\"name\":\"Kolya\",\"age\":\"25\",\"sex\":\"M\"}}]}]},{\"Odessa\":[{\"Chornomorsk\":[{\"person5\":{\"name\":\"Dina\",\"age\":\"25\",\"sex\":\"F\"}},{\"person6\":{\"name\":\"David\",\"age\":\"25\",\"sex\":\"M\"}}]},{\"Ismail\":[{\"person7\":{\"name\":\"Edna\",\"age\":\"25\",\"sex\":\"F\"}},{\"person8\":{\"name\":\"Abram\",\"age\":\"25\",\"sex\":\"M\"}}]}]}]}}")},
                {4, (JsonObject) new JsonParser().parse("{ \"Ukraine\": { \"capital\": \"Kiev\", \"mayor\": \"Petro\", \"regions\": [{ \"Dnipro\": [{ \"Novomoskovsk\": [{ \"person1\": { \"name\": \"Nadya\", \"age\": \"25\", \"sex\": \"F\" } }, { \"person2\": { \"name\": \"Vasya\", \"age\": \"25\", \"sex\": \"M\" } }] }, { \"Nikopol\": [{ \"person3\": { \"name\": \"Valya\", \"age\": \"25\", \"sex\": \"F\" } }, { \"person4\": { \"name\": \"Kolya\", \"age\": \"25\", \"sex\": \"M\" } }] }] }] } }")},
                {0, (JsonObject) new JsonParser().parse("{ \"Ukraine\": { \"capital\": \"Kiev\"}}")},
        };
    }
}
